#
# Be sure to run `pod lib lint data-structures.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DataStructures'
  s.version          = '0.1.0'
  s.summary          = 'Stack, queue, DFS and BFS data structures'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://bitbucket.org/cs3217/2017-ps1-a0135817b'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Zhang Yijiang' => 'mediumdeviation@gmail.com' }
  s.source           = { :git => 'git@bitbucket.org:cs3217/2017-ps1-a0135817b.git', :branch => 'cocoapod', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'
  s.source_files = 'DataStructures/*.swift'
end
